<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::resource ('artiste', 'ArtisteController');
Route::resource ('film', 'FilmController');
Route::resource ('cinema', 'CinemaController');
Route::resource ('salle', 'SalleController');
Route::resource ('seance', 'SeanceController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@userLogout')->name('logout');
