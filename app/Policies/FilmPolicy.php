<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\ { Movie, Cinema, Projection, Room, Artist };

class FilmPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function manage(User $user, Movie $film)
    {
        return $user->id === $film->user_id;
    }

    public function update(User $user, Movie $film)
    {
        return $user->id === $film->user_id;
    }

    public function edit(User $user, Movie $film)
    {
        return $user->id === $film->user_id;
    }

    public function destroy(User $user, Movie $film)
    {
        return $user->id === $film->user_id;
    }
}
