<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CinemaPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function manage(User $user, Cinema $cinema)
    {
        return $user->id === $cinema->user_id;
    }

    public function update(User $user, Cinema $cinema)
    {
        return $user->id === $cinema->user_id;
    }

    public function edit(User $user, Cinema $cinema)
    {
        return $user->id === $cinema->user_id;
    }

    public function destroy(User $user, Cinema $cinema)
    {
        return $user->id === $cinema->user_id;
    }
}
