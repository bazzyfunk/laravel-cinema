<?php

namespace App\Policies;

use App\User;
use App\Models\Artiste;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArtistePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function manage(User $user, Artiste $artiste)
    {
        return $user->id === $artiste->user_id;
    }

    public function update(User $user, Artiste $artiste)
    {
        return $user->id === $artiste->user_id;
    }

    public function edit(User $user, Artiste $artiste)
    {
        return $user->id === $artiste->user_id;
    }

    public function destroy(User $user, Artiste $artiste)
    {
        return $user->id === $artiste->user_id;
    }
}
