<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Seance;
use App\Models\Salle;
use App\Models\Film;
use App\Http\Requests\SeanceRequest;
use App\Notifications\SeanceCreated;

class SeanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('seances.index', [ 'seances' => \App\Models\Seance::paginate(3) ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('seances.edit', ['seance' => Seance::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SeanceRequest $request)
    {
        $seance = Seance::create($request->all());

        Auth()->user()->notify(new SeanceCreated($seance));

        return redirect ()->route ('home')
                          ->with ('ok', __('La séance a bien été enregistré'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Seance $seance)
    {
        return view('seances.edit', ['seance' => $seance, 'films' => Film::all(), 'salles' => Salle::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SeanceRequest $request, Seance $seance)
    {
        $seance->update( $request->all() );

        return redirect()->route('seance.index')
                         ->with( 'ok', __('La seance a bien été modifiée'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Salle $salle)
    {
        $salle->delete();
        return response()->json();
    }

    public function __construct()
    {
        $this->middleware('ajax')->only('destroy');
    }
}
