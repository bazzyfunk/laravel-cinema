<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $fillable = [
        'titre', 'annee', 'artiste_id'
    ];

    public function realisateur()
    {
        return $this->belongsTo('App\Models\Artiste');
    }

    public function acteurs() {
        return $this->belongsToMany('App\Models\Artiste');
    }

    public function film_seance() {
        return $this->belongsToOne('App\Models\Seance');
    }
}
