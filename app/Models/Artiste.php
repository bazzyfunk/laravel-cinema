<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Artiste extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'nom', 'prenom', 'annee_naissance'
    ];

    public function films_realises()
    {
        return $this->hasMany('App\Models\Film');
    }

    public function films_joues()
    {
        return $this->belongsToMany('App\Models\Film')->withPivot('nom_role');
    }
}
