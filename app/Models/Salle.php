<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salle extends Model
{
    protected $fillable = [
        'nom_cinema', 'climatise', 'capacite'
    ];

    /**
     * Relation Salle - Cinema
     * Relation belongsTo, car
     * une salle appartient à
     * un seul complexe
     */
    public function cinema() {
        return $this->belongsTo('App\Models\Cinema');
    }

    public function salle_seance() {
        return $this->belongsToOne('App\Models\Seance');
    }
}
