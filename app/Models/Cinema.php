<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cinema extends Model
{
    protected $fillable = [
        'nom_cinema', 'arrondissement', 'adresse'
    ];

    /**
     * Relation Cinema -> Salle
     * Il s'agit d'une relation hasMany,
     * car un Cinema peut avoir plusieurs
     * salles de cinéma
     */
    public function salles() {
        return $this->hasMany('App\Models\Salle');
    }
}
