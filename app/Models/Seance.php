<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seance extends Model
{
    protected $fillable = [
        'dateDebut', 'dateFin'
    ];

    public function cinema() {
        return $this->belongsTo('App\Models\Cinema');
    }

    public function film() {
        return $this->belongsTo('App\Models\Film');
    }

    public function salle() {
        return $this->belongsTo('App\Models\Salle');
    }
}
