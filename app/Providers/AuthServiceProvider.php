<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\Artiste;
use App\Models\Film;
use App\Policies\ArtistePolicy;
use App\Policies\FilmPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected $policies = [
        Artiste::class => ArtistePolicy::class,
        Film::class => FilmPolicy::class,
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Gate::resource('artistes', ArtistePolicy::class);
        Gate::resource('films', FilmPolicy::class);
    }
}
