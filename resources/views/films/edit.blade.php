@extends('layouts.app')

@section('title', 'Éditer un film')

@section('content')

<form method="POST" action="{{ route('film.update',$film->id) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <div class="form-group">
        <label for="titre">Titre</label>
        <input type="text" class="form-control" name="titre" id="titre" value="" required />
        @if ($errors->has('titre'))
        <div class="invalid-feedback">
            {{ $errors->first('titre') }}
        </div>
        @endif
    </div>
    <div class="form-group">
        <label for="annee">Année de sortie</label>
        <input type="number" class="form-control" name="annee" id="annee" value="" />
        @if ($errors->has('annee'))
        <div class="invalid-feedback">
            {{ $errors->first('annee')}}
        </div>
        @endif
    </div>
    <div class="form-group">
        <label for="realisateur">Réalisateur</label>
        <select class="custom-select" name="artiste_id" id="realisateur">
            @foreach ($artistes as $artiste)
                <option value="{{ $artiste->id }}">{{$artiste->nom}} {{$artiste->prenom}}</option>
            @endforeach
        </select>
        @if ($errors->has('id'))
        <div class="invalid-feedback">
            {{ $errors->first('id')}}
        </div>
        @endif
    </div>

    <button type="submit" class="btn btn-primary">Créer</button>
</form>
@endsection
