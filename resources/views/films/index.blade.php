@extends('layouts.app')

@section('title', 'Éditer un film')

@section('content')
<table class="table table-striped table-centered">
    <thead>
        <tr>
            <th>{{ __('Titre') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($films as $film)
            <tr>
            <td>{{ $film->titre }}</td>
                <td class="table-action">
                    <a role="button" href="{{ route('film.edit', $film->id) }}" class="btn btn-primary tn-sm" data-toggle="tooltip" title="@lang('Modifier le titre') {{ $film->titre }}">
                        <i class="fas fa-edit fa-lg"></i>
                        Editer
                    </a>
                    <a role="button" href="{{ route('film.destroy', $film->id) }}" class="btn btn-danger tn-sm" data-toggle="tooltip" title="@lang('Supprimer le film') {{ $film->titre }}">
                        <i class="fas fa-trash fa-lg"></i>
                        Supprimer
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

{{--
---- Ceci permet de créer une
---- pagination pour notre
---- affichage
--}}
{{ $films->appends(request()->except('page'))->links() }}
<a role="button" href="{{ route('film.create')}}" class="btn btn-primary btn-lg" data-toggle="tooltip" title="@lang('Créer un film')">
    <i class="fas fa-edit fa-lg"></i>
    Créer
</a>
@endsection
