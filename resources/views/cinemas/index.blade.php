@extends('layouts.app')

@section('title', 'Listing des cinémas')

@section('content')
<table class="table table-striped table-centered">
    <thead>
        <tr>
            <th>{{ __('Nom du cinéma') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($cinemas as $cinema)
            <tr>
                <td>{{ $cinema->nom_cinema }}</td>
                <td class="table-action">
                    <a role="button" href="{{ route('cinema.edit', $cinema->id) }}" class="btn btn-primary tn-sm" data-toggle="tooltip" title="@lang('Modifier le cinema') {{ $cinema->nom_cinema }}">
                        <i class="fas fa-edit fa-lg"></i>
                        Editer
                    </a>
                    <a role="button" href="{{ route('cinema.destroy', $cinema->id) }}" class="btn btn-danger tn-sm" data-toggle="tooltip" title="@lang('Supprimer le cinema') {{ $cinema->nom_cinema }}">
                        <i class="fas fa-trash fa-lg"></i>
                        Supprimer
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

{{--
---- Ceci permet de créer une
---- pagination pour notre
---- affichage
--}}
{{ $cinemas->appends(request()->except('page'))->links() }}
<a role="button" href="{{ route('cinema.create')}}" class="btn btn-primary btn-lg" data-toggle="tooltip" title="@lang('Créer un cinema')">
    <i class="fas fa-edit fa-lg"></i>
    Créer
</a>
@endsection
