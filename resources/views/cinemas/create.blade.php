@extends('layouts.app')

@section('title', 'Créer un cinéma')

@section('content')

<form method="POST" action="{{ route('cinema.store') }}">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="nom_cinema">Nom Cinéma</label>
        <input type="text" class="form-control" name="nom_cinema" id="nom_cinema" value="" required />
        @if ($errors->has('nom_cinema'))
        <div class="invalid-feedback">
            {{ $errors->first('nom_cinema')}}
        </div>
        @endif
    </div>
    <div class="form-group">
        <label for="arrondissement">Arrondissement</label>
        <input type="number" class="form-control" name="arrondissement" id="arrondissement" value="" />
        @if ($errors->has('arrondissement'))
        <div class="invalid-feedback">
            {{ $errors->first('arrondissement')}}
        </div>
        @endif
    </div>
    <div class="form-group">
        <label for="adresse">Adresse</label>
        <input type="text" class="form-control" name="adresse" id="adresse" value="" />
        @if ($errors->has('adresse'))
        <div class="invalid-feedback">
            {{ $errors->first('adresse')}}
        </div>
        @endif
    </div>

    <button type="submit" class="btn btn-primary">Créer</button>
</form>
@endsection
