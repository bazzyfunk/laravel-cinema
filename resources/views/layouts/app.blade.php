<html>
    <head>
        <title>@yield('title')</title>
        <link href="{{ asset('css/app.css') }}?v={{ filemtime(public_path('css/app.css')) }}" rel="stylesheet" type="text/css" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        <header>
            <h1 class="text-center m-4">🎬 Cinema Laravel Project 🎬</h1>
        </header>
        <div class="container">

            <div class="text-center m-4">
            <a href="{{ url('/') }}" class="btn btn-secondary btn-lg m-2">Homepage</a>

            @if (Route::has('login'))
                @auth
                <a href="{{ route('logout') }}" class="btn btn-secondary btn-lg m-2">
                    Logout
                </a>

                @else
                    @if (Route::has('logout'))
                    <a href="{{ route('login') }}" class="btn btn-secondary btn-lg m-2">
                        Login
                    </a>
                    @endif
                    @if (Route::has('register'))
                        <a href="{{ route('register') }}" class="btn btn-secondary btn-lg m-2">Register</a>
                    @endif
                @endauth
            </div>
            @endif

            @yield('content')
        </div>

        <script src="{{ asset('js/app.js')}}?v={{ filemtime(public_path('js/app.js')) }}"></script>
    </body>
</html>
