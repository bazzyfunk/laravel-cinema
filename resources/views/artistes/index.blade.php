@extends('layouts.app')

@section('content')
<table class="table table-striped table-centered">
    <thead>
        <tr>
            <th>{{ __('Nom') }}</th>
            <th>{{ __('Prenom') }}</th>
            <th>{{ __('Année de naissance') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($artistes as $artiste)
            <tr>
            <td>{{ $artiste->nom }}</td>
            <td>{{ $artiste->prenom }}</td>
            <td>{{ $artiste->annee_naissance }}</td>
            <td>
                <a role="button" href="{{ route('artiste.edit', $artiste->id) }}" class="btn btn-primary tn-sm" data-toggle="tooltip" title="@lang('Modifier l\'artiste') {{ $artiste->nom }}">
                    <i class="fas fa-edit fa-lg"></i>
                    Editer
                </a>
                <a role="button" href="{{ route('artiste.destroy', $artiste->id) }}" class="btn btn-danger tn-sm" data-toggle="tooltip" title="@lang('Supprimer l\'artiste') {{ $artiste->nom }}">
                    <i class="fas fa-trash fa-lg"></i>
                    Supprimer
                </a>
            </td>
            </tr>
        @endforeach
    </tbody>
</table>

{{--
---- Ceci permet de créer une
---- pagination pour notre
---- affichage
--}}
{{ $artistes->appends(request()->except('page'))->links() }}

<a role="button" href="{{ route('artiste.create')}}" class="btn btn-primary btn-lg" data-toggle="tooltip" title="@lang('Créer un artiste')">
    <i class="fas fa-edit fa-lg"></i>
    Créer
</a>
@endsection
