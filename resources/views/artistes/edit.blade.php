@extends('layouts.app')

@section('title', 'Page Title')

@section('content')

<form method="POST" action="{{ route('artiste.update',$artiste->id) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}

    <div class="form-group">
        <label for="nom">Nom</label>
        <input type="text" class="form-control" name="nom" id="nom" value="" required />
        @if ($errors->has('nom'))
        <div class="invalid-feedback">
            {{ $errors->first('nom')}}
        </div>
        @endif
    </div>
    <div class="form-group">
        <label for="prenom">Prenom</label>
        <input type="text" class="form-control" name="prenom" id="prenom" value="" />
        @if ($errors->has('prenom'))
        <div class="invalid-feedback">
            {{ $errors->first('prenom')}}
        </div>
        @endif
    </div>
    <div class="form-group">
        <label for="annee_naissance">Année de naissance</label>
        <input type="number" class="form-control" name="annee_naissance" id="annee_naissance" value="" />
        @if ($errors->has('annee_naissance'))
        <div class="invalid-feedback">
            {{ $errors->first('annee_naissance')}}
        </div>
        @endif
    </div>

    <button type="submit" class="btn btn-primary">Créer</button>
</form>
@endsection
