@extends('layouts.app')

@section('title', 'Créer une seance')

@section('content')

<form method="POST" action="{{ route('seance.store')}}">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="film_id">Film</label>
        <select class="custom-select" name="film_id" id="titre">
            @foreach ($films as $film)
                <option value="{{ $film->id }}">{{$film->titre}}</option>
            @endforeach
        </select>
        @if ($errors->has('id'))
        <div class="invalid-feedback">
            {{ $errors->first('id')}}
        </div>
        @endif
    </div>

    <div class="form-group">
        <label for="dateDebut">Début</label>
        <input type="datetime" name="dateDebut" id="dateDebut" value="" required/>
        @if ($errors->has('dateDebut'))
        <div class="invalid-feedback">
            {{$errors->first('dateDebut')}}
        </div>
        @endif
    </div>

    <div class="form-group">
        <label for="dateFin">Start Time</label>
        <input type="datetime" name="dateFin" id="dateFin" value="" required/>
        @if ($errors->has('dateFin'))
        <div class="invalid-feedback">
            {{$errors->first('dateFin')}}
        </div>
        @endif
    </div>

    <div class="form-group">
        <label for="salle_id">Salle</label>
        <select class="custom-select" name="salle_id" id="nom_salle">
            @foreach ($salles as $salle)
                <option value="{{ $salle->id }}">{{$salle->nom_salle}}</option>
            @endforeach
        </select>
        @if ($errors->has('id'))
        <div class="invalid-feedback">
            {{ $errors->first('id')}}
        </div>
        @endif
    </div>

    <button type="submit" class="btn btn-primary">Créer</button>
</form>
@endsection
