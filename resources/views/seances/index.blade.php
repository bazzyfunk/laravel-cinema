@extends('layouts.app')

@section('title', 'Listing des seances')

@section('content')
<table class="table table-striped table-centered">
    <thead>
        <tr>
            <th>{{ __('Film') }}</th>
            <th>{{ __('Début') }}</th>
            <th>{{ __('Fin') }}</th>
            <th>{{ __('Salle') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($seances as $seance)
            <tr>
                <td>{{ $seance->film()->first()->titre }}</td>
                <td>{{ $seance->dateDebut }}</td>
                <td>{{ $seance->dateFin }}</td>
                <td>{{ $seance->salle()->first()->nom_salle }}</td>
                <td class="table-action">
                    <a role="button" href="{{ route('seance.edit', $seance->id) }}" class="btn btn-primary tn-sm" data-toggle="tooltip" title="@lang('Modifier la') {{ $seance->nom_salle }}">
                        <i class="fas fa-edit fa-lg"></i>
                        Editer
                    </a>
                    <a role="button" href="{{ route('seance.destroy', $seance->id) }}" class="btn btn-danger tn-sm" data-toggle="tooltip" title="@lang('Supprimer la') {{ $seance->nom_salle }}">
                        <i class="fas fa-trash fa-lg"></i>
                        Supprimer
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

{{--
---- Ceci permet de créer une
---- pagination pour notre
---- affichage
--}}
{{ $seances->appends(request()->except('page'))->links() }}
<a role="button" href="{{ route('seance.create')}}" class="btn btn-primary btn-lg" data-toggle="tooltip" title="@lang('Créer une seance')">
    <i class="fas fa-edit fa-lg"></i>
    Créer
</a>
@endsection
