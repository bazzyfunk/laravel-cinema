@extends('layouts.app')

@section('title', 'Listing des salles')

@section('content')
<table class="table table-striped table-centered">
    <thead>
        <tr>
            <th>{{ __('Cinema') }}</th>
            <th>{{ __('Nom') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($salles as $salle)
            <tr>
            <td>{{ $salle->cinema()->first()->nom_cinema }}</td>
                <td>{{ $salle->nom_salle }}</td>
                <td class="table-action">
                    <a role="button" href="{{ route('salle.edit', $salle->id) }}" class="btn btn-primary tn-sm" data-toggle="tooltip" title="@lang('Modifier la') {{ $salle->nom_salle }}">
                        <i class="fas fa-edit fa-lg"></i>
                        Editer
                    </a>
                    <a role="button" href="{{ route('salle.destroy', $salle->id) }}" class="btn btn-danger tn-sm" data-toggle="tooltip" title="@lang('Supprimer la') {{ $salle->nom_salle }}">
                        <i class="fas fa-trash fa-lg"></i>
                        Supprimer
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

{{--
---- Ceci permet de créer une
---- pagination pour notre
---- affichage
--}}
{{ $salles->appends(request()->except('page'))->links() }}
<a role="button" href="{{ route('salle.create')}}" class="btn btn-primary btn-lg" data-toggle="tooltip" title="@lang('Créer une salle')">
    <i class="fas fa-edit fa-lg"></i>
    Créer
</a>
@endsection
