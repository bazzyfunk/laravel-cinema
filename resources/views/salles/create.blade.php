@extends('layouts.app')

@section('title', 'Créer une salle')

@section('content')

<form method="POST" action="{{ route('salle.store')}}">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="nom_salle">Nom salle</label>
        <input type="text" class="form-control" name="nom_salle" id="nom_salle" value="" required />
        @if ($errors->has('nom_salle'))
        <div class="invalid-feedback">
            {{ $errors->first('nom_salle') }}
        </div>
        @endif
    </div>
    <div class="form-group">
        <label for="capacite">Capacité</label>
        <input type="number" class="form-control" name="capacite" id="capacite" value="" required />
        @if ($errors->has('capacite'))
        <div class="invalid-feedback">
            {{ $errors->first('capacite') }}
        </div>
        @endif
    </div>
    <div class="form-group">
        <label for="cinema_id">Cinema</label>
        <select class="custom-select" name="cinema_id" id="nom_cinema">
            @foreach ($cinemas as $cinema)
                <option value="{{ $cinema->id }}">{{$cinema->nom_cinema}}</option>
            @endforeach
        </select>
        @if ($errors->has('id'))
        <div class="invalid-feedback">
            {{ $errors->first('id')}}
        </div>
        @endif
    </div>

    <button type="submit" class="btn btn-primary">Créer</button>
</form>
@endsection
