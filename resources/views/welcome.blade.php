<!doctype html>
@extends('layouts.app')

@section('title', 'Page Title')

@section('content')

<div class="text-center">
    <a href="{{ url('/film')}}" class="btn btn-lg btn-primary">Films</a>
    <a href="{{ url('/cinema')}}" class="btn btn-lg btn-primary">Cinema</a>
    <a href="{{ url('/salle')}}" class="btn btn-lg btn-primary">Salle</a>
    <a href="{{ url('/seance')}}" class="btn btn-lg btn-primary">Seance</a>
    <a href="{{ url('/artiste')}}" class="btn btn-lg btn-primary">Artiste</a>
</div>

@endsection
