<?php

use Illuminate\Database\Seeder;

class SallesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('salles')->insert([
            [
                'nom_salle' => 'Salle 1',
                'climatisation' => 1,
                'capacite' => 200,
                'cinema_id' => 4,
            ],
            [
                'nom_salle' => 'Salle Spielberg',
                'climatisation' => 1,
                'capacite' => 300,
                'cinema_id' => 5,
            ],
            [
                'nom_salle' => 'Salle 2',
                'climatisation' => 0,
                'capacite' => 80,
                'cinema_id' => 6,
            ],
            [
                'nom_salle' => 'Salle Kubrick',
                'climatisation' => 0,
                'capacite' => 110,
                'cinema_id' => 5,
            ],
            [
                'nom_salle' => 'Salle 3',
                'climatisation' => 1,
                'capacite' => 250,
                'cinema_id' => 6,
            ],
            [
                'nom_salle' => 'Salle Lucas',
                'climatisation' => 1,
                'capacite' => 300,
                'cinema_id' => 5,
            ],
            [
                'nom_salle' => 'Salle Leone',
                'climatisation' => 0,
                'capacite' => 130,
                'cinema_id' => 4,
            ],
        ]);
    }
}
