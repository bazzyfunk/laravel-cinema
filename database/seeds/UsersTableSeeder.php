<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'joao',
                'email' => 'joaoantonio.mendesdacosta@crea-inseec.com',
                'password' => 'joao'
            ],[
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => 'admin',
            ]
        ]);
    }
}
