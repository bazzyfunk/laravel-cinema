<?php

use Illuminate\Database\Seeder;

class ArtistesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artistes')->insert([
            [
                'nom' => 'Hardy',
                'prenom' => 'Tom',
                'annee_naissance' => 1972
            ],
            [
                'nom' => 'Miyazaki',
                'prenom' => 'Hayao',
                'annee_naissance' => 1941,
            ],
            [
                'nom' => 'Allen',
                'prenom' => 'Woody',
                'annee_naissance' => 1938,
            ],
            [
                'nom' => 'Lasseter',
                'prenom' => 'John',
                'annee_naissance' => 1957,
            ],
            [
                'nom' => 'Winding Refn',
                'prenom' => 'Nicolas',
                'annee_naissance' => 1970,

            ],
            [
                'nom' => 'Gosling',
                'prenom' => 'Ryan',
                'annee_naissance' => 1980,
            ],
            [
                'nom' => 'Mulligan',
                'prenom' => 'Carey',
                'annee_naissance' => 1985,
            ],
        ]);
    }
}
