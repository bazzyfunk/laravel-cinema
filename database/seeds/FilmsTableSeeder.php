<?php

use Illuminate\Database\Seeder;
use App\Models\Artiste;
use App\Models\Film;

class FilmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $film = new Film([
            'titre' => 'Mon Voisin Totoro',
            'annee' => 1988,
            'artiste_id' => 1,
        ]);
        $film->save();

        $realisateur = Artiste::where('nom','Miyazaki')->where('prenom', 'Hayao')->first();
        $film->artiste_id = $realisateur->id;
        $film->save();

        $film = new Film([
            'titre' => 'Mad Max: Fury Road',
            'annee' => 2015,
            'artiste_id' => 1,
        ]);
        $film->save();

        $acteur = Artiste::where('nom', 'Hardy')->where('prenom','Tom')->first();
        $film->acteurs()->attach($acteur->id, ['nom_role' => 'Max']);

        $film = new Film([
            'titre' => 'Toy Story',
            'annee' => 1996,
            'artiste_id' => 1,
        ]);
        $film->save();

        $realisateur = Artiste::where('nom','Lasseter')->where('prenom', 'John')->first();
        $film->artiste_id = $realisateur->id;
        $film->save();

        $film = new Film([
            'titre' => 'Drive',
            'annee' => 2011,
            'artiste_id' => 1,
        ]);
        $film->save();

        $realisateur = Artiste::where('nom','Winding Refn')->where('prenom', 'Nicolas')->first();
        $film->artiste_id = $realisateur->id;

        $acteur = Artiste::where('nom', 'Gosling')->where('prenom','Ryan')->first();
        $film->acteurs()->attach($acteur->id, ['nom_role' => 'The Driver']);

        $acteur = Artiste::where('nom', 'Mulligan')->where('prenom','Carey')->first();
        $film->acteurs()->attach($acteur->id, ['nom_role' => 'Irene']);
        $film->save();
    }
}
