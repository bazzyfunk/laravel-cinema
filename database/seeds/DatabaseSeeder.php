<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ArtistesTableSeeder::class);
        $this->call(FilmsTableSeeder::class);
        $this->call(CinemasTableSeeder::class);
        $this->call(SallesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SeancesTableSeeder::class);
    }
}
