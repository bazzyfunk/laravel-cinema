<?php

use Illuminate\Database\Seeder;

class CinemasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cinemas')->insert([
            [
                'nom_cinema' => 'Mulberry',
                'arrondissement' => '1801',
                'adresse' => 'Parkersburg, WV 26101',
            ],
            [
                'nom_cinema' => 'Kino Midland',
                'arrondissement' => '1202',
                'adresse' => '107 SW. Poplar Street',
            ],
            [
                'nom_cinema' => 'Pathe Balexert',
                'arrondissement' => '1204',
                'adresse' => '12 rue des Marroniers',
            ],
            [
                'nom_cinema' => 'Arena Cinema',
                'arrondissement' => '1227',
                'adresse' => '20 rue des Acacias',
            ],
            [
                'nom_cinema' => 'Pathe Rex',
                'arrondissement' => '1201',
                'adresse' => '4 rue de la Confédération',
            ],
            [
                'nom_cinema' => 'Nord Sud',
                'arrondissement' => '1202',
                'adresse' => '34 rue de la Servette',
            ],[
                'nom_cinema' => 'Cinema Harvey',
                'arrondissement' => '1202',
                'adresse' => '509 Harvey Street ',
            ]
        ]);
    }
}
