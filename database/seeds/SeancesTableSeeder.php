<?php

use Illuminate\Database\Seeder;
use App\Models\Seance;
use App\Models\Salle;
use App\Models\Film;

class SeancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seance = new Seance([
            'dateDebut' => '2019-04-21 12:00:00',
            'dateFin' => '2019-04-21 14:30:00',
            'film_id' => Film::where('titre','Mad Max: Fury Road')->first()->id,
            'salle_id' => Salle::where('nom_salle','Salle Spielberg')->first()->id
        ]);
        $seance->save();

        $seance = new Seance([
            'dateDebut' => '2019-04-23 21:00:00',
            'dateFin' => '2019-04-23 23:15:00',
            'film_id' => Film::where('titre','Toy Story')->first()->id,
            'salle_id' => Salle::where('nom_salle','Salle 1')->first()->id
        ]);
        $seance->save();

        $seance = new Seance([
            'dateDebut' => '2019-04-21 12:00:00',
            'dateFin' => '2019-04-21 14:30:00',
            'film_id' => Film::where('titre','Drive')->first()->id,
            'salle_id' => Salle::where('nom_salle','Salle Leone')->first()->id
        ]);
        $seance->save();
    }
}
