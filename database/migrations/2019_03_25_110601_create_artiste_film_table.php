<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtisteFilmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artiste_film', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom_role', 18);
            $table->bigInteger('film_id')->unsigned()->nullable();
            $table->foreign('film_id')->references('id')->on('films');
            $table->bigInteger('artiste_id')->unsigned()->nullable();
            $table->foreign('artiste_id')->references('id')->on('artistes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artiste_film');
    }
}
